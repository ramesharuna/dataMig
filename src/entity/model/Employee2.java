package entity.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employee2")
public class Employee2 {
@Id
@Column(name="id")
private int empno;
private String ename;
private String sal;
public Employee2(){}
public int getEmpno() {
	return empno;
}
public void setEmpno(int empno) {
	this.empno = empno;
}
public String getEname() {
	return ename;
}
public void setEname(String ename) {
	this.ename = ename;
}
public String getSal() {
	return sal;
}
public void setSal(String sal) {
	this.sal = sal;
}
}
