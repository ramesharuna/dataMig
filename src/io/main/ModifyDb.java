package io.main;

import io.dao.HibernateDaoImpl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ModifyDb {

	private static ApplicationContext context;
	
	public static void main(String[] args) { 
		context=new ClassPathXmlApplicationContext("spring.xml");
		HibernateDaoImpl obj=context.getBean("hibernateDaoImpl", HibernateDaoImpl.class);
		obj.getResult();
	}

}
