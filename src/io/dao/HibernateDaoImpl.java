package io.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import entity.model.Employee;


@Repository
public class HibernateDaoImpl {
	@Autowired
	private SessionFactory sourceSessionFactory;
	@Autowired
	private SessionFactory destSessionFactory;

	public SessionFactory getSourceSessionFactory() {
		return sourceSessionFactory;
	}

	public void setSourceSessionFactory(SessionFactory sourceSessionFactory) {
		this.sourceSessionFactory = sourceSessionFactory;
	}

	public SessionFactory getDestSessionFactory() {
		return destSessionFactory;
	}

	public void setDestSessionFactory(SessionFactory destSessionFactory) {
		this.destSessionFactory = destSessionFactory;
	}

	public void getResult() {
		SessionFactory sourceSessionFactory = getSourceSessionFactory();
		Session sourceSession = sourceSessionFactory.openSession();		
		Session destSession = getDestSessionFactory().openSession();
		Transaction tx= destSession.beginTransaction();
		 Map<String, ClassMetadata> allClassMetadata = sourceSessionFactory.getAllClassMetadata();
	    for(String entityName : allClassMetadata.keySet()){
	        SessionFactoryImpl sfImpl = (SessionFactoryImpl) sourceSessionFactory;
	        String tableName = ((AbstractEntityPersister)sfImpl.getEntityPersister(entityName)).getTableName();
	        System.out.println(entityName + "\t" + tableName);
	        
	        Query query = sourceSession.createQuery("from "+entityName);
			List<Employee> list=(List<Employee>)query.list();
			for(Employee e:list){
				destSession.saveOrUpdate(e);
			}			
	    }
	    tx.commit();
	}
}
